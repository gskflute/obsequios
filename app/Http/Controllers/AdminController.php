<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Category;
use App\Product;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function index() {
        $categories = Category::all();
        $categories->toarray();

        $products = Product::all();
        $products->toarray();
        
        return view('layouts.adminPanel', array('categories' =>  $categories, 'products' => $products));
	}
    

}
