@section('main')

<h1>Create Category</h1>

{{ Form::open(array('route' => 'categories.store')) }}
    <ul>

        
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        
            {{ Form::submit('Save', array('class' => 'btn')) }}
        
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">Error!</li>')) }}
    </ul>
@endif
