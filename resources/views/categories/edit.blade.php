@section('main')

<h1>Edit Category</h1>
{{ Form::model($category, array('method' => 'PATCH', 'route' => array('categories.update', $category->id))) }}
    <ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        
        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            <a href="{{ url('/admin') }}">Cancel</a>

        </li>
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif
