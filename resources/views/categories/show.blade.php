<!DOCTYPE html>
<html>
  <head>
    <title>Categories:</title>
    <a href="{{ url('admin') }}">Back to panel</a>
  </head>
  <body>
  @foreach ($categories as $cat)
    <h1>Category:</h1>
    <ul>
      <li>Name: {{ $cat->name }}</li>
    </ul>
@endforeach
  </body>
</html>