<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Admin Panel</title>
        {!! Html::style('assets/css/bootstrap.css') !!}
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }


            td {
                font-size: 15px;
                color: black;
                font-weight: 600;

            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                @section('main')


                <h1>All Categories</h1>
                <a href="{{ url('product/create') }}">Add new product</a>
                <a href="{{ url('categories/create') }}">Add new category</a>

                @if ($categories->count())
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($categories as $cat)
                                <tr>
                                    <td>{{ $cat->name }}</td>
                                    <td><a class="btn btn-info" href="{{ route('categories.edit', ['id' => $cat->id]) }}">Edit</a></td>
                                    <!--<td><a class="btn btn-danger" href="{{  route('categories.destroy', ['id' => $cat->id]) }}">Delete</a></td>-->
                                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('categories.destroy', $cat->id))) }}                       
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                            {{ Form::close() }}
                                    </td>
                                </tr>-
                            @endforeach
                            
                        </tbody>
                    
                    </table>
                @else
                    There are no categories
                @endif


                <h1>All Products</h1>

                @if ($products->count())
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($products as $prod)
                                <tr>
                                    <td>{{ $prod->title }}</td>
                                    <td>{{ $prod->category }}</td>
                                    <td>{{ $prod->description }}</td>
                                    <td>{{ $prod->price }}</td>
                                    <td><a class="btn btn-info" href="{{ route('product.edit', ['id' => $prod->id]) }}">Edit</a></td>
                                    <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('product.destroy', $prod->id))) }}                       
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                            {{ Form::close() }}
                                    </td>
                                </tr>-
                            @endforeach
                            
                        </tbody>
                    
                    </table>
                @else
                    There are no products
                @endif
