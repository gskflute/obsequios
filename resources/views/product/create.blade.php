@section('main')

<h1>Create Product</h1>

{{ Form::open(array('route' => 'product.store')) }}
    <ul>

        
          <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>
        <li>
            {{ Form::label('category', 'Category:') }}
            {{ Form::text('category') }}
        </li>
        <li>
            {{ Form::label('description', 'Description:') }}
            {{ Form::text('description') }}
        </li>
        <li>
            {{ Form::label('price', 'Price:') }}
            {{ Form::text('price') }}
        </li>
        
        
            {{ Form::submit('Save', array('class' => 'btn')) }}
        
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">Error!</li>')) }}
    </ul>
@endif
