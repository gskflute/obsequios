@section('main')

<h1>Edit Product</h1>
{{ Form::model($product, array('method' => 'PATCH', 'route' => array('product.update', $product->id))) }}
    <ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>
        <li>
            {{ Form::label('category', 'Category:') }}
            {{ Form::text('category') }}
        </li>
        <li>
            {{ Form::label('description', 'Description:') }}
            {{ Form::text('description') }}
        </li>
        <li>
            {{ Form::label('price', 'Price:') }}
            {{ Form::text('price') }}
        </li>
        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            <a href="{{ url('/admin') }}">Cancel</a>

        </li>
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif
