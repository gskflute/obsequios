<!DOCTYPE html>
<html>
  <head>
    <title>Products:</title>
    <a href="{{ url('admin') }}">Back to panel</a>
  </head>
  <body>
  @foreach ($products as $prod)
    <h1>Products:</h1>
    <ul>
      <li>Title: {{ $prod->title }}</li>
      <li>Category: {{ $prod->category }}</li>
      <li>Description: {{ $prod->description }}</li>
      <li>Price: {{ $prod->price }}</li>
    </ul>
@endforeach
  </body>
</html>